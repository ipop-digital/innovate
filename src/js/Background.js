import inview from "./utils/inview";

export default function Background(el) {
  const bg = document.querySelector(".bg");

  console.log("hello");

  inview.set(el, {
    enter() {
      bg.style.backgroundColor = el.dataset.bg;
    },
    leave() {},
  });

  return () => {};
}
