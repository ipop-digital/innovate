import inview from "./utils/inview";

export default function InviewToggle(el) {
  inview.set(el, {
    enter() {
      console.log("enter");
      el.classList?.add(el.dataset?.class);
    },
    leave() {
      console.log("leave");
      el.classList?.remove(el.dataset?.class);
    },
  });

  return () => {};
}
