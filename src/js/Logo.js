export default function Logo(el) {
  const rects = el.querySelectorAll("rect");

  let index = 0;
  let colorIndex = 0;

  function getColor(index) {
    let color = "";
    if (index == 0) {
      color = "#81FBCF";
      if (Math.random() * 100 < 10) {
        color = "#30EAA8";
      }
    } else {
      color = "#FA056F";
      if (Math.random() * 100 < 10) {
        color = "#D8005F";
      }
    }
    return color;
  }

  const tick = () => {
    rects[index].style.fill = getColor(colorIndex);
    if (index < rects.length - 1) {
      index++;
    } else {
      index = 0;
      if (colorIndex < 1) {
        colorIndex++;
      } else {
        colorIndex = 0;
      }
    }
  };

  setInterval(tick, 50);

  return () => {};
}
