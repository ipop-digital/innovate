import { Curtains, Plane } from "curtainsjs";
import { wobble } from "./utils/shaders";
const shader = wobble;

// set our initial parameters (basic uniforms)
const params = {
  vertexShader: shader.vertex, // our vertex shader ID
  fragmentShader: shader.fragment, // our framgent shader ID
  widthSegments: 10,
  heightSegments: 10, // we now have 40*40*6 = 9600 vertices !
  premultiplyAlpha: true,
  uniforms: {
    time: {
      name: "uTime",
      type: "1f",
      value: 0,
    },
    direction: {
      name: "uDirection",
      type: "1f",
      value: 0,
    },
  },
};

export default function TeamWebgl(el) {
  const curtains = new Curtains({
    container: "canvas",
    watchScroll: true,
  });

  const planeElements = [...el.querySelectorAll(".plane")];
  const planeImg = el.querySelectorAll(".plane img");
  planeImg.forEach((img) => (img.src = img.dataset?.src));

  const planes = planeElements.map((el) => new Plane(curtains, el, params));

  planes.forEach((plane) => {
    plane.onRender(() => {
      // use the onRender method of our plane fired at each requestAnimationFrame call
      // plane.uniforms.direction.time = window.vector ?? 0;
      plane.uniforms.direction.value = window.vector ?? 0;
      plane.updatePosition();
    });
  });

  return () => {};
}
