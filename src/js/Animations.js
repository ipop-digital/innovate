import { gsap } from "gsap/dist/gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import eventBus from "./utils/eventBus";
gsap.registerPlugin(ScrollTrigger);

export default function Animations(el) {
  let tls = [];

  let tl = gsap.timeline({
    // yes, we can add it to an entire timeline!
    scrollTrigger: {
      trigger: ".projects-list",
      pin: false, // pin the trigger element while active
      start: "top center", // when the top of the trigger hits the top of the viewport
      end: "10%", // end after scrolling 500px beyond the start
      scrub: 1, // smooth scrubbing, takes 1 second to "catch up" to the scrollbar
    },
  });

  tl.to(
    ".projects-list",
    0.1,
    {
      backgroundColor: "#3C3C3B",
    },
    "first"
  );
  tl.fromTo(
    ".projects-list__card",
    {
      scale: 0.8,
      opacity: 0,
    },
    {
      scale: 1,
      opacity: 1,
    }
  );

  tls.push(tl);

  let proxy = { skew: 0 },
    skewSetter = gsap.quickSetter(".projects-list__card", "skewY", "deg"), // fast
    clamp = gsap.utils.clamp(-20, 20); // don't let the skew go beyond 20 degrees.

  ScrollTrigger.create({
    onUpdate: (self) => {
      let skew = clamp(self.getVelocity() / -500);
      // only do something if the skew is MORE severe. Remember, we're always tweening back to 0, so if the user slows their scrolling quickly, it's more natural to just let the tween handle that smoothly rather than jumping to the smaller skew.
      if (Math.abs(skew) > Math.abs(proxy.skew)) {
        proxy.skew = skew;
        gsap.to(proxy, {
          skew: 0,
          duration: 0.8,
          ease: "power3",
          overwrite: true,
          onUpdate: () => skewSetter(proxy.skew),
        });
      }
    },
  });

  // make the right edge "stick" to the scroll bar. force3D: true improves performance
  gsap.set(".projects-list__card", {
    transformOrigin: "center center",
    force3D: true,
  });

  let tl2 = gsap
    .timeline({
      // yes, we can add it to an entire timeline!
      scrollTrigger: {
        trigger: ".marquee-section",
        // pin: true, // pin the trigger element while active
        start: "top bottom", // when the top of the trigger hits the top of the viewport
        end: "200%", // end after scrolling 500px beyond the start
        scrub: 1, // smooth scrubbing, takes 1 second to "catch up" to the scrollbar
      },
    })
    .to(
      ".marquee1",
      {
        x: "-50%",
      },
      "one"
    )
    .fromTo(
      ".marquee2",
      {
        x: "-50%",
      },
      {
        x: "0%",
      },
      "one"
    )
    .to(
      ".marquee3",
      {
        x: "-40%",
      },
      "one"
    );

  tls.push(tl2);

  document.querySelectorAll(".logos .logo").forEach((logo) => {
    let ltl = gsap
      .timeline({
        scrollTrigger: {
          trigger: logo,
          start: "top bottom",
          markers: false,
          scrub: 1,
        },
      })
      .fromTo(
        logo,
        {
          opacity: 0,
        },
        {
          opacity: 1,
          scale: 1,
        }
      );

    tls.push(ltl);
  });

  // gsap
  //   .timeline({
  //     scrollTrigger: {
  //       trigger: ".logos",
  //       start: "top bottom-=10%",
  //       end: "5%",
  //       scrub: 1,
  //     },
  //   })
  //   .fromTo(
  //     ".logos-inner",
  //     {
  //       translateY: "100%",
  //       opacity: 0,
  //     },
  //     {
  //       translateY: "0%",
  //       opacity: 1,
  //     }
  //   );

  let tl3 = gsap
    .timeline({
      scrollTrigger: {
        trigger: ".shutters",
        scrub: 1,
        // start: "top bottom",
        // end: "100%",
        markers: false,
      },
    })
    .fromTo(
      ".shutters",
      {
        translateX: "0%",
      },
      {
        translateX: "-100%",
      }
    );

  tls.push(tl3);

  eventBus.on("scrolltrigger:refresh", () => ScrollTrigger.refresh());

  return () => {
    tls.forEach((tl) => tl?.kill?.());
  };
}
