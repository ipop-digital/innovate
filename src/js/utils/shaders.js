export const wobble = {
  vertex: `    
      #ifdef GL_ES
      precision mediump float;
      #endif
      
      #define PI 3.14159265359
      
      // those are the mandatory attributes that the lib sets
      attribute vec3 aVertexPosition;
      attribute vec2 aTextureCoord;
    
      // those are mandatory uniforms that the lib sets and that contain our model view and projection matrix
      uniform mat4 uMVMatrix;
      uniform mat4 uPMatrix;
    
      uniform mat4 planeTextureMatrix;
    
      // if you want to pass your vertex and texture coords to the fragment shader
      varying vec3 vVertexPosition;
      varying vec2 vTextureCoord;
    
      varying float vDirection;
    
      uniform float uDirection;
    
      void main() {
          vec3 position = aVertexPosition;
    
          float x = sin((position.y * 0.5 - 0.5) * PI) * uDirection;
    
          position.x -= x;
          
          gl_Position = uPMatrix * uMVMatrix * vec4(position, 1.0);
    
          // set the varyings
          vTextureCoord = (planeTextureMatrix * vec4(aTextureCoord, 0., 1.)).xy;
          vVertexPosition = position;
    
          vDirection = uDirection;
      }`,
  fragment: `
      #ifdef GL_ES
      precision mediump float;
      #endif
    
      #define PI2 6.28318530718
      #define PI 3.14159265359
      #define S(a,b,n) smoothstep(a,b,n)
      
      // get our varyings
      varying vec3 vVertexPosition;
      varying vec2 vTextureCoord;
    
      // the uniform we declared inside our javascript
      uniform float uTime;
    
      // our texture sampler (default name, to use a different name please refer to the documentation)
      uniform sampler2D planeTexture;
      
      varying float vDirection;
    
      void main(){
          vec2 uv = vTextureCoord;
    
          float scale = -abs(vDirection) * 0.8;
    
          uv = (uv - 0.5) * scale + uv;
    
          float r = texture2D(planeTexture, vec2(uv.x - vDirection * 0.2, uv.y)).r;
          float g = texture2D(planeTexture, vec2(uv.x - vDirection * 0.4, uv.y)).g;
          float b = texture2D(planeTexture, vec2(uv.x - vDirection * 0.5, uv.y)).b;
          
          gl_FragColor = vec4(r, g, b, 1.0);
      }
      `,
};
