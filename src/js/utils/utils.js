const refs = [];

export const domToggle = (el) => {
  let refIndex;
  const foundRef = refs.find((ref, i) => {
    refIndex = i;
    return ref.el.isSameNode(el);
  });

  if (el.isConnected) {
    let reference = {
      el,
    };
    if (el.nextSibling) {
      reference.relation = el.nextSibling;
      reference.relationType = "nextSibling";
    } else if (el.previousSibling) {
      reference.relation = el.nextSibling;
      reference.relationType = "previousSibling";
    } else {
      reference.relation = el.parentElement;
      reference.relationType = "parent";
    }
    refs.push(reference);
    el.parentElement.removeChild(el);
  } else if (foundRef !== undefined) {
    switch (foundRef.relationType) {
      case "nextSibling":
        foundRef.relation.parentElement.insertBefore(el, foundRef.relation);
        break;
      case "previousSibling":
        foundRef.relation.parentElement.insertAfter(el, foundRef.relation);
      default:
        foundRef.relation.parentElement.appendChild(el);
        break;
    }

    refs.splice(refIndex, 1);
  }
};

export const qa = (el) => document.querySelectorAll(el);
