import eventBus from "./utils/eventBus";

export default function ProjectCard(el) {
  eventBus.on("scroll", () => {
    el.classList.add("clip");

    setTimeout(() => {
      el.classList.remove("clip");
    }, 200);
  });

  return () => {};
}
