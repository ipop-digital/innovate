import { gsap } from "gsap/dist/gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import { qa } from "./utils/utils";
gsap.registerPlugin(ScrollTrigger);
import events from "./utils/events";

export default function Header(el) {
  const { addEvent, removeEvents } = events;
  // const menuBtn = el.querySelector(".menu-btn");
  // const menu = el.querySelector("nav");

  // addEvent("click", menuBtn, (e) => {
  //   const styles = window.getComputedStyle(menu);
  //   console.log(styles.getPropertyValue("display"));
  //   if (styles.getPropertyValue("display") == "none") {
  //     gsap.to(menu, {
  //       display: "block",
  //       opacity: 1,
  //     });
  //   } else {
  //     gsap.to(menu, {
  //       opacity: 0,
  //       display: "none",
  //     });
  //   }
  // });

  qa(".dark-bg").forEach((darkbg) => {
    ScrollTrigger.create({
      trigger: darkbg,
      start: "top top",
      onToggle: (self) => {
        console.log("toggle");
        el.classList.toggle("text-white");
      },
    });
  });

  gsap
    .timeline({
      // yes, we can add it to an entire timeline!
      scrollTrigger: {
        trigger: "main",
        start: "top top", // when the top of the trigger hits the top of the viewport
        end: "200px", // end after scrolling 500px beyond the start
        scrub: 1, // smooth scrubbing, takes 1 second to "catch up" to the scrollbar
      },
    })
    .fromTo(
      el,
      {
        paddingTop: "30px",
        paddingLeft: "3em",
        paddingRight: "3em",
      },
      {
        paddingTop: "15px",
        paddingLeft: "1.5em",
        paddingRight: "1.5em",
      },
      "first"
    )
    .fromTo(
      "#header .logo-wrap",
      {
        width: "100px",
      },
      {
        width: "80px",
      },
      "first"
    )
    .to(
      ".header-hide",
      {
        opacity: 0,
        display: "none",
      },
      "first"
    );

  return () => {};
}
