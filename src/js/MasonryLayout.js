import Masonry from "masonry-layout";

export default function MasonryLayout(el) {
  var msnry = new Masonry(el, {
    // options...
    itemSelector: "[data-action='MasonryLayout'] > div",
    // horizontalOrder: true,
    columnWidth: 0,
    fitWidth: true,
  });
  return () => {};
}
