import barba from "@barba/core";
import "core-js/stable";
import "regenerator-runtime/runtime";
import lazySizes from "lazysizes";
import "./app.scss";
import Logo from "./js/Logo";
import Slider from "./js/Slider";
import TextAnim from "./js/TextAnim";
import InviewToggle from "./js/InviewToggle";
// import TeamWebgl from "./js/TeamWebgl";
// import ProjectCard from "./js/ProjectCard";
import eventBus from "./js/utils/eventBus";
import ProjectList from "./js/ProjectList";
import Animations from "./js/Animations";
import Form from "./js/Form";
import Header from "./js/Header";
import { gsap } from "gsap/dist/gsap";
// import MasonryLayout from "./js/MasonryLayout";

let actions = {};
let actionChain = new Map();
function registerAction(action) {
  actions[action.name] = {
    mount: action,
  };
}

registerAction(Logo);
registerAction(TextAnim);
registerAction(Slider);
registerAction(InviewToggle);
registerAction(ProjectList);
// registerAction(MasonryLayout);
registerAction(Animations);
registerAction(Form);
registerAction(Header);
// registerAction(TeamWebgl);

barba.init({
  debug: true,
  logLevel: "error",
  timeout: 5000,
  transitions: [
    {
      name: "opacity-transition",
      leave(data) {
        return gsap.to(data.current.container, {
          duration: 0.1,
          opacity: 0,
        });
      },
      enter(data) {
        return gsap.from(data.next.container, {
          duration: 0.1,
          opacity: 0,
        });
      },
    },
  ],
});

barba.hooks.beforeEnter((data) => {
  clearActions();
});

barba.hooks.after((data) => {
  runActions();

  const hash = window?.location?.hash;
  const target = document.querySelector(hash.length ? hash : undefined);
  if (target) {
    window.scroll(0, target.getBoundingClientRect().top);
  } else {
    window.scroll(0, 0);
  }
  document.querySelector("video")?.play();
});

function runActions(root = document) {
  root.querySelectorAll("[data-action]").forEach((el) => {
    const action = el.dataset.action;
    let actionId = el.dataset.id;

    if (!actionChain.get(actionId)) {
      actionId = `a${0 | (Math.random() * 10000)}`;
      el.dataset.id = actionId;

      if (actions[action]) {
        actionChain.set(actionId, {
          el,
          unmount: actions[action].mount(el)?.bind?.({}),
        });
      }
    }
  });
  const mobbtn = document.querySelector(".menu-btn");
  const meganav = document.querySelector(".mega-nav");

  const closeBtn = document.querySelector(".close-btn");
  if (closeBtn) {
    closeBtn.addEventListener("click", function () {
      meganav.classList.remove("open");
    });
  }
  mobbtn?.addEventListener("click", function () {
    console.log("hello");
    meganav.classList.add("open");
  });
  meganav.querySelectorAll("a").forEach((a) =>
    a.addEventListener("click", () => {
      if (a.href.indexOf("#") !== -1) meganav.classList.toggle("open");
    })
  );
}

function clearActions(root = document.body) {
  actionChain.forEach((ac, key) => {
    if (root.contains(ac.el)) {
      ac?.unmount?.();
      actionChain.delete(key);
    }
  });
}

runActions();

// throw new Error();
window.addEventListener("scroll", (e) => {
  eventBus.emit("scroll", e);
});

document.addEventListener("lazyloaded", () =>
  eventBus.emit("scrolltrigger:refresh")
);
