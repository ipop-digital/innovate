// tailwind.config.js
// See more information about this file at https://tailwindcss.com/docs/installation#create-your-configuration-file

module.exports = {
  // prefix: 'tw-',
  important: false,
  mode: "jit",
  purge: {
    mode: "all",
    enabled: true,
    content: [
      "./src/**/*.js",
      "./templates/**/*.twig",
      "./templates/**/*.html",
      "./templates/*.html",
      "./templates/*.twig",
    ],
    options: {
      keyframes: true,
      fontFace: true,
    },
  },
  darkMode: "media", // See https://tailwindcss.com/docs/dark-mode
  theme: {
    fontSize: {},
    extend: {
      outline: {
        gold: ["1px solid #D6A249", "0px"],
      },
      flex: {
        2: 2,
        2.5: 2.5,
      },
      zIndex: {
        "-1": -1,
      },
      width: {},
      fontFamily: {},
      colors: {
        offwhite: "#F7F7F7",
        text: "#3C3C3B",
        lime: "#81FBCF",
        grey: {
          DEFAULT: "#939393",
          light: "#EEF0EF",
          dark: "#3C3C3B",
        },
        pink: "#FA056F",
        yellow: "#ECF700",
      },
    },
  },
  variants: {},
  plugins: [require("@tailwindcss/aspect-ratio")],
};
